-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql310.epizy.com
-- Generation Time: Dec 16, 2019 at 04:10 AM
-- Server version: 5.6.45-86.1
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_24547307_portal_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 NOT NULL,
  `image` varchar(128) CHARACTER SET utf8 NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(11, 'wahyu', 'wahyufaturrizkyy@gmail.com', 'default.png', '$2y$10$zH5zx383cysdtrVNDLwgIen0ZBM0W7mpcFNRhjSCcWpNavXUO1kSS', 2, 1, 1569586288),
(12, 'admin', 'wahyubocahbijak@gmail.com', 'default.png', '$2y$10$eVYYCxQI2T3HPb1nXVP8wu88V0MxAb6cX2Y7ead8qepHo8C7muw3i', 1, 1, 1569670536),
(13, 'budi', 'budi@gmail.com', 'default.png', '$2y$10$F5bMDMMdu3I/Y8eLeOLDcO.W0beWET6L6AOShGF1BjeneGH24RpPa', 2, 1, 1569734995),
(14, 'fatir', 'fatir@gmail.com', 'default.png', '$2y$10$lixtrAzmlqTS9azpCiOzTu0j0e7I0Jr.ly1ph.Sc31m.IQtVK81Hm', 2, 1, 1569746596),
(15, 'dila', 'dila@gmail.com', 'default.png', '$2y$10$dSYSp82QF88yKCrhDJCDHuCYpry9RJbfc9YqV8igl4mpSIENmUnYC', 2, 1, 1570648749),
(16, 'sekar', 'sekar@gmail.com', 'default.png', '$2y$10$K0Lm09sUcmAFnS7WpQRLq.OYMQ96tGzPmxTkRvIg6H/YIyfXUHlb6', 2, 1, 1570650027),
(17, 'Fatur', 'Faturganggu@ppk.com', 'default.png', '$2y$10$pAq4V5oGjCeMj5i5PrEvg.IdF7YWU8jGLXNoAOWvhArLldaueV8Wi', 2, 1, 1570667446),
(18, 'kpufasilkomtiusu', 'pepek@mail.com', 'default.png', '$2y$10$Ozwz61yjumbhmUQpIKJxOuShVvz1JPKIxFLpSYcg4YJ3G/2XAMqq.', 2, 1, 1571560040),
(19, 'Indriyana', 'indri@gmail.com', 'default.png', '$2y$10$UlQB2tqA/MXa6uasUiPaze.9J89FUWBmgGHANerK3JM.DsLk1GY6O', 2, 1, 1571643388);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 1, 3),
(11, 2, 3),
(18, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `date_created_menu` int(11) NOT NULL,
  `update_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `date_created_menu`, `update_menu`) VALUES
(1, 'Admin', 1569771831, 1569771831),
(2, 'User', 1569771831, 1569771831),
(3, 'Menu', 1569771831, 1569771831);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `date_created_role` int(11) NOT NULL,
  `updated_role` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `date_created_role`, `updated_role`) VALUES
(1, 'Administrator', 1569914991, 1569914991),
(2, 'member', 1569914991, 1569914991);

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) CHARACTER SET utf8 NOT NULL,
  `url` varchar(128) CHARACTER SET utf8 NOT NULL,
  `icon` varchar(128) CHARACTER SET utf8 NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created_sub_menu` int(11) NOT NULL,
  `updated_sub_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`, `date_created_sub_menu`, `updated_sub_menu`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1, 0, 0),
(2, 2, 'My Profile', 'user', 'fas fa-fw fa-user-circle', 1, 0, 0),
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1, 0, 0),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder-plus', 1, 0, 0),
(5, 3, 'Sub Menu Management', 'menu/submenu', 'fas fa-fw fa-file-alt', 1, 0, 0),
(15, 1, 'Role Management', 'admin/role', 'fas fa-fw fa-address-book', 1, 1570093192, 1570112722);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
